;
;    wayless, an operating system built in Rust
;    Copyright (C) Waylon Cude 2019
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, under version 2 of the License
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

global context_switch
;NOTE: this may cause races because both the syscall yield and
; timer interrupt use this
;global segments
;global return_to_thread
global segment_lock
extern syscall_lock
extern switch_stack
extern switch_thread
extern pml4

%include "thread.mac"
%include "defs.mac"
%include "print.mac"

section .data
thread_switch_lock:
    dq 0

segment_lock:
    dq 0

context:
    times 18 dq 0
context_end:
    dq 0 ; context is actually 19 elements long

segments:
    .cs: dw 0
    .ss: dw 0
    .ds: dw 0
    .es: dw 0
    .fs: dw 0
    .gs: dw 0


section .text
[bits 64]
context_switch:
    
    ; maybe todo: make sure stack is okay

    ; get lock to context datastructure
    ; maybe ues cmpxchg
    .acquire:
        lock bts qword [rel thread_switch_lock],0
        jc .acquire

    CONTEXT_SAVE

    mov rdi, context
    mov rsi, segments

    ; Switch to the kernel page table so we can use the kernel heap
    mov rsp, pml4
    mov cr3, rsp
    ; We trashed the stack via context save so get to a good stack
    mov rsp, switch_stack
    call switch_thread

    ; Switch to new page table
     mov cr3,rax

    CONTEXT_RESTORE context

    ; Maybe todo: figure out how to get rid of this push+pop
    push rsi
    mov rsi,APIC
    ; reset apic 
    mov dword [rsi+0xB0], 0
    pop rsi

    ; release lock
    ; maybe use fences
    ; or make this atomic somehow
    mov qword [rel thread_switch_lock], qword 0
    mfence
    ;jmp [rsp]

    iretq

;return_to_thread:
;    ; Switch to new page table
;     mov cr3,rsi
;
;    CONTEXT_RESTORE context
;
;    ; release syscall lock
;    mov qword [rel syscall_lock], qword 0
;
;    mfence
;    ;jmp [rsp]
;
;    iretq


; vim: ft=nasm
