#TODO: make nasm detect which arch once we support multiarch

TARGET = build/wayless.bin
RELEASE_TARGET = build/wayless.opt.bin

#NASM = nasm -felf64 -i src/arch/x86-64/ -d kernel_size=`cat $(SIZE)`
NASM = nasm -felf64 -i src/arch/x86-64/

X86_64_SOURCES = apic.asm syscall.asm thread.asm
X86_64_DEPS = print.mac thread.mac defs.mac
RLIBS = libwayless.rlib
OPT_RLIBS = libwayless.rlib.opt
LINKSCRIPT := linker.ld #Huh, I need a different linkscript for each target
LINKFLAGS := -T $(LINKSCRIPT)
#LINKFLAGS += --gc-sections
LINKFLAGS += -Map build/map.txt
LINKFLAGS += -L ./
LINKFLAGS += -nostdlib
LINKFLAGS += -z max-page-size=0x1000
LINKFLAGS += --gc-sections
ISO = build/wayless.iso
RELEASE_ISO = build/wayless.opt.iso

QEMU = qemu-system-x86_64 
QEMUFLAGS += -device isa-debug-exit,iobase=0xf4,iosize=0x04
QEMUFLAGS += -cpu max
QEMUFLAGS += -nographic
#QEMUFLAGS += -serial stdio
QEMUFLAGS += -m 512
QEMU_RUNFLAGS := -enable-kvm

#seL4 location
SEL4 = sel4/sel4
SEL4_INCLUDE = $(SEL4)/include
SEL4_BUILD = sel4/build

RUST_TARGET_PATH=${CURDIR}

#TODO: set these dynamically
TARGET_TRIPLE = x86_64-elf
SOURCES = $(X86_64_SOURCES)
ARCH_PATH = src/arch/x86-64
XARGO_FLAGS += --target $(TARGET_TRIPLE)
AR = ar #x86_64-elf-ar
LD = ld #x86_64-elf-ld
RLOC=target/x86_64-elf/debug
RLOC_OPT=target/x86_64-elf/release

OBJECTS:=$(patsubst %,build/%.o,$(basename $(SOURCES)))
SOURCES:=$(patsubst %,$(ARCH_PATH)/%,$(X86_64_SOURCES))
ASM_DEPS:=$(patsubst %,$(ARCH_PATH)/%,$(X86_64_DEPS))
RLIBS:=$(patsubst %,build/%,$(RLIBS))
OPT_RLIBS:=$(patsubst %,build/%,$(OPT_RLIBS))
BOOTLESS = bootless/build/bootless.a

# This has to be an absolute path
XARGO_HOME=${CURDIR}/target/xargo

# NOTE: this is also hardcoded into the kernel
USERSPACE=userspace
USERSPACE_START=$(USERSPACE)/start

BINDINGS=build/bindings.rs

CARGOS = Cargo.toml.build Cargo.toml.gen_bindings

all: $(SOURCES) $(TARGET)

release: $(RELEASE_TARGET)

build: 
	mkdir -p build

$(TARGET): $(OBJECTS) $(RLIBS) linker.ld $(USERSPACE_START) $(CARGOS) $(BOOTLESS)
	$(LD) -o $@ $(LINKFLAGS) $(OBJECTS) $(RLIBS) $(BOOTLESS)

$(RELEASE_TARGET): $(OBJECTS) $(OPT_RLIBS) linker.ld $(USERSPACE_START) $(CARGOS) $(BOOTLESS)
	$(LD) -o $@ $(LINKFLAGS) $(OBJECTS) $(OPT_RLIBS) $(BOOTLESS) --strip-debug

# Initialize the size file with a sensible guess
#$(SIZE): | build
#	echo $(DEFAULT_SIZE) > $(SIZE)

$(OBJECTS): | build

$(USERSPACE_START): FORCE
	make -C $(USERSPACE)

build/libwayless.rlib: $(RLOC)/libwayless.a
	ln -sf ${CURDIR}/$^ $@

build/libwayless.rlib.opt: $(RLOC_OPT)/libwayless.a
	ln -sf ${CURDIR}/$^ $@

bootless/Makefile:
	git submodule update --init --recursive

$(BOOTLESS): bootless/Makefile
	make -C bootless


#NOTE: This maybe should be FORCE
$(BINDINGS): include/gen_headers gen_bindings.rs | build
	cp Cargo.toml.gen_bindings Cargo.toml
	cargo run

$(RLOC)/libwayless.a: $(USERSPACE_START) $(BINDINGS) FORCE
	cp Cargo.toml.build Cargo.toml
	XARGO_HOME=$(XARGO_HOME) RUST_TARGET_PATH=$(RUST_TARGET_PATH) xargo build $(XARGO_FLAGS) 

$(RLOC_OPT)/libwayless.a: $(USERSPACE_START) $(BINDINGS) FORCE
	cp Cargo.toml.build Cargo.toml
	XARGO_HOME=$(XARGO_HOME) RUST_TARGET_PATH=$(RUST_TARGET_PATH) xargo build $(XARGO_FLAGS) --release


#test: src/kernel.rs build/libcore.rlib src/*.rs
#	$(RUSTC) --cfg feature=\"test\" $(RFLAGS) $< -o build/kernel.o

build/%.o: $(ARCH_PATH)/%.asm $(ASM_DEPS)
	$(NASM) $< -o $@

$(ISO): $(TARGET) iso/boot/grub/grub.cfg
	cp $(TARGET) iso/boot/wayless.bin
	grub-mkrescue -o $@ iso

$(RELEASE_ISO): $(RELEASE_TARGET) iso/boot/grub/grub.cfg
	cp $(RELEASE_TARGET) iso/boot/wayless.bin
	grub-mkrescue -o $@ iso

#iso/%: %
	#ln -sf ${CURDIR}/$< iso/boot/wireos.bin
	#cp $< $@

print: 
	echo $(OBJECTS)

#libwaylos:
#	xargo rustc --target x86_64-elf.json --verbose -- -L .

clean:
	rm -f build/*.o
	rm -f build/*.rlib
	rm -f build/*.bin
	rm -f build/*.iso
	rm -f build/map.txt
	rm -f $(BINDINGS)
	rm -f $(SIZE)
	make -C bootless clean

distclean:
	rm -rf build
	rm -rf target
	rm -rf include
	git submodule deinit --all -f
	rm -rf $(SEL4_BUILD)

gdb: $(ISO)
	#This is in shell so I can save the PID for cleaning up the qemu
	# process
	$(QEMU) -cdrom $^ $(QEMUFLAGS) -s -S &
	gdb
	#TODO: figure out how to clean up the running qemu process

debug: $(ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) -s -S || true

test: FORCE $(BINDINGS) $(USERSPACE_START)
	cp Cargo.toml.build Cargo.toml
	echo -e "\n[dev-dependencies]\nproptest = \"0.9\"" >> Cargo.toml
	cargo test

with-kvm: $(ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) $(QEMU_RUNFLAGS) || true

run: $(ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) || true

run-release: $(RELEASE_ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) || true

run-release-kvm: $(RELEASE_ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) $(QEMU_RUNFLAGS) || true

nographic: $(ISO)
	$(QEMU) -cdrom $^ $(QEMUFLAGS) -display none || true

include: $(SEL4_INCLUDE)
	cp -r $^ ${CURDIR}/include


#include/syscall.h: include sel4/build
#	cp $(SEL4_BUILD)/gen_headers/arch/api/syscall.h ${CURDIR}/include

include/gen_headers: include sel4/build/gen_headers
	cp -r $(SEL4_BUILD)/gen_headers ${CURDIR}/include

sel4/build/gen_headers: $(SEL4_INCLUDE)
	mkdir -p sel4/build
	cd sel4/build && cmake -DCROSS_COMPILER_PREFIX= -DCMAKE_TOOLCHAIN_FILE=../sel4/gcc.cmake -G Ninja -C ../sel4/configs/X64_verified.cmake ../sel4/ \
	&& ninja


$(SEL4_INCLUDE):
	git submodule update --init

FORCE: ;
