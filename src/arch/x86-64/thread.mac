;
;    wayless, an operating system built in Rust
;    Copyright (C) Waylon Cude 2019
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, under version 2 of the License
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

; macro to save registers to a user stack

%define user_stack 0

%macro SAVE_REGS_SEL4 0
    extern registers_end
    ; We need to build this struct
    ; We do this via a series of pushes
    ; Why do we save all of these?
    mov rsp, registers_end
    push rcx ; NextIP
    push r11 ; RFLAGS
    push r15 ; Message Reg
    push r9  ; Message Reg
    push r8  ; Message Reg
    push r10 ; Message Reg
    push rdx ; syscall number
    push r14
    push r13
    push r12
    push rbp
    push rbx
    push rax
    push rsi ; msgInfo
    push rdi ; cap register
%endmacro

%macro SAVE_REGS 0
    extern registers_end
    ; We need to build this struct
    ; We do this via a series of pushes
    ; Why do we save all of these?
    mov rsp, registers_end
    push r15
    push r14
    push r13
    push r12
    push 0 ; skip r11
    push r10
    push r9
    push r8
    push rbp
    push rdi
    push rsi
    push rdx
    push 0 ; skip rcx
    push rbx
    push rax
    push r11
    push 0 ; skip rsp
    push rcx
%endmacro

%macro RESTORE_REGS 0
    extern registers
    ; We need to extract registers from this struct
    mov rsp, registers
    pop rcx ;instruction pointer
    add rsp, 8; skip stack pointer
    pop r11 ;rflags
    pop rax
    pop rbx
    add rsp, 8; skip rcx
    pop rdx
    pop rsi
    pop rdi
    pop rbp
    pop r8
    pop r9
    pop r10
    add rsp, 8; skip r11
    pop r12
    pop r13
    pop r14
    pop r15
%endmacro

%macro RESTORE_REGS_SEL4 0
    extern registers
    ; We need to extract registers from this struct
    mov rsp, registers
    pop rdi
    pop rsi
    pop rax
    pop rbx
    pop rbp
    pop r12
    pop r13
    pop r14
    pop rdx
    pop r10
    pop r8
    pop r9
    pop r15
    ; NOTE: we might not need to pop these because they are clobbers
    ; Maybe we should zero them out to avoid leaking information?
    pop r11
    pop rcx
%endmacro

%macro CONTEXT_SAVE 0
    ; first save rax so we can use it to grab stuff off of the stack
    mov [rel context+3*8], rax
    ; first save the instruction pointer,
    ; the stack pointer,
    ; the segments
    ; and rflags
    pop rax
    mov [rel context], rax
    pop rax
    mov [rel segments.cs], rax ; save cs
    pop rax
    mov [rel context+2*8], rax ; save rflags
    pop rax
    mov [rel context+1*8], rax ; save rsp
    pop rax
    mov [rel segments.ss], rax ; save ss

    mov ax,ds
    mov [rel segments.ds], ax ; save ds
    mov ax,es
    mov [rel segments.es], ax ; save es
    mov ax,fs
    mov [rel segments.fs], ax ; save fs
    mov ax,gs
    mov [rel segments.gs], ax ; save gs

    ; We need to build this struct
    ; We do this via a series of pushes
    mov rsp, context_end
    push r15
    push r14
    push r13
    push r12
    push r11
    push r10
    push r9
    push r8
    push rbp
    push rdi
    push rsi
    push rdx
    push rcx
    push rbx
    ; We don't need to do any extra pushes here because we
    ; alread saved rip,rsp, and rflags
%endmacro

%macro CONTEXT_RESTORE 1

    ; We need to extract registers from this struct
    mov rsp, %1
    add rsp, 8*4; skip first 4 entries

    pop rbx
    pop rcx
    pop rdx
    pop rsi
    pop rdi
    pop rbp
    pop r8
    pop r9
    pop r10
    pop r11
    pop r12
    pop r13
    pop r14
    pop r15

    ;Clear rax because for some reason mov ds,ax expands to mov ds,eax
    xor rax,rax
    mov ax, word [rel segments.ds] ; restore ds
    mov ds, word [rel segments.ds] ; restore ds
    ;mov ds, word [rel segments.ds] ; restore ds
    ;mov ds,ax
    mov es, word [rel segments.es] ; restore es
    ;mov es,ax
    mov fs, word [rel segments.fs] ; restore fs
    ;mov fs,ax
    mov gs, word [rel segments.gs] ; restore gs
    ;mov gs,ax

    mov rsp, switch_stack
    movsx rax, word [rel segments.ss] ; restore ss
    push rax
    mov rax, [rel %1+1*8] ; restore rsp
    push rax
    mov rax, [rel %1+2*8] ; restore rflags
    push rax
    movsx rax, word [rel segments.cs] ; restore cs
    push rax
    mov rax, [rel %1] ; restore rip
    push rax

    ; and restore rax
    mov rax, [rel %1+3*8]

%endmacro
%macro DOLOCK 1
    .acquire:
        lock bts qword [rel %1],0
        jc .acquire
%endmacro
%macro UNLOCK 1
    mov qword [rel %1], qword 0
%endmacro
; vim: ft=nasm
