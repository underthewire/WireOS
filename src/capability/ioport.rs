//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use crate::invocation::InvocationLabel;

use crate::error::Error;

pub fn handle_io_invocation(label: InvocationLabel, _length: u64, registers: &mut [u64; 4], start: u64, end: u64) -> Result<(),Error> {
    // Check if the port is out of bounds
    // If it is return with an error
    let port = registers[0];
    if port < start || port > end {
        return Err(Error::IllegalOperation)
    }
    match label {
        //Outb
        InvocationLabel::X86IOPortOut8 => {
            unsafe {
                asm!("outb %al, %dx" 
                     : 
                     : "{dx}" (registers[0]), "{al}" (registers[1])
                     :
                     : "volatile" );}
        },
        //Output 16-bit word
        InvocationLabel::X86IOPortOut16 => {
            unsafe {
                asm!("outw %ax, %dx" 
                     : 
                     : "{dx}" (registers[0]), "{ax}" (registers[1])
                     :
                     : "volatile" );}
        },
        //Output 32-bit double-word
        InvocationLabel::X86IOPortOut32 => {
            unsafe {
                asm!("outl %eax, %dx" 
                     : 
                     : "{dx}" (registers[0]), "{eax}" (registers[1])
                     :
                     : "volatile" );}
        },
        // Input an 8-bit byte
        InvocationLabel::X86IOPortIn8 => {
            unsafe {
                asm!("inb %dx, %al" 
                     : "={al}" (registers[0])
                     : "{dx}" (port)
                     :
                     : "volatile" );}
        },
        // Input a 16-bit word
        InvocationLabel::X86IOPortIn16 => {
            unsafe {
                asm!("inw %dx, %ax" 
                     : "={ax}" (registers[0])
                     : "{dx}" (port)
                     :
                     : "volatile" );}
        },
        // Input a 32-bit doubleword
        InvocationLabel::X86IOPortIn32 => {
            unsafe {
                asm!("inl %dx, %eax" 
                     : "={eax}" (registers[0])
                     : "{dx}" (port)
                     :
                     : "volatile" );}
        },
        _ => {
            // TODO: Is this the right error?
            return Err(Error::IllegalOperation)
        }
    }
    Ok(())
}
