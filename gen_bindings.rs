extern crate bindgen;

use std::path::PathBuf;

fn main() {
    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        .use_core()
        .ctypes_prefix("crate::std::os::raw")
        .clang_arg("-I./include")
        .clang_arg("-I./include/arch/x86")
        .clang_arg("-I./include/64/")
        .clang_arg("-I./include/gen_headers")
        .header("include/arch/x86/arch/types.h")
        .header("include/gen_headers/arch/api/syscall.h")
        .header("include/gen_headers/api/invocation.h")
        .header("include/gen_headers/arch/api/invocation.h")
        .header("include/gen_headers/arch/api/sel4_invocation.h")
        .blacklist_type("syscall_t")
        //We don't want to generate any functions
        .ignore_functions()
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from("build/");
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
